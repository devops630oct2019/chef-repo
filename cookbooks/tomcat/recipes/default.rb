#
# Cookbook:: tomcat
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.

secret = Chef::EncryptedDataBagItem.load_secret("/etc/chef/encrypted_data_bag_secret")
tomcat_keys = Chef::EncryptedDataBagItem.load("tomcatbag", "tomcatitem", secret)


include_recipe 'java'

directory node['tomcat']['tomcathomedir'] do
  owner node['tomcat']['sudouser']
  group node['tomcat']['sudouser']
  mode '0755'
  action :create
end

user node['tomcat']['user'] do
  comment node['tomcat']['user']
  home node['tomcat']['tomcathomedir']
  system true
  shell '/bin/false'
end

group node['tomcat']['group'] do
  action :create
end

ark node['tomcat']['user'] do 
  url node['tomcat']['downloadurl']
  home_dir node['tomcat']['tomcatdownloaddir']
  prefix_root '/opt'
  owner node['tomcat']['user']
  group node['tomcat']['user']
end

directory node['tomcat']['tomcathomedir'] do
  owner node['tomcat']['user']
  group node['tomcat']['user']
  mode '0755'
  recursive true
end

template "/opt/tomcat/apache-tomcat/conf/tomcat-users.xml" do
  variables(:tomcatappuser => tomcat_keys['appuser'], :tomcatapppassword => tomcat_keys['apppassword'],:tomcatappuser1 => tomcat_keys['appuser1'],:tomcatapppassword1 => tomcat_keys['apppassword1'])
  source 'tomcat-users.xml'
  owner node['tomcat']['user']
  group node['tomcat']['user']
  mode '0644'
  notifies :restart, 'service[tomcat]', :delayed
end

template "/opt/tomcat/apache-tomcat/webapps/manager/META-INF/context.xml" do
  source 'context.xml'
  owner node['tomcat']['user']
  group node['tomcat']['user']
  mode '0644'
  notifies :restart, 'service[tomcat]', :delayed
end

template "/etc/systemd/system/tomcat.service" do
  source 'tomcat.service'
  owner node['tomcat']['sudouser']
  group node['tomcat']['sudouser']
  mode '0755'
  notifies :enable, 'service[tomcat]', :delayed
  notifies :restart, 'service[tomcat]', :delayed
end

service 'tomcat' do
  supports restart: true
  action :enable
end


  
